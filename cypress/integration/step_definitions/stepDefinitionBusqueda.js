import { And, Given, Then, When } from "cypress-cucumber-preprocessor/steps";
import Busqueda from "../page-objects/busquedaPageObjects";

const busquedaPage = new Busqueda();

Given("yo navego a la pagina de busqueda", () => {
  cy.visit('https://www.madrid.es/portal/site/munimadrid');
  cy.get(`ul.main-menu li [href*='contacto.html']`).click();
  cy.get(`ul.sub-menu.visible-md-block.visible-lg-block li [href*='Direcciones-y-telefonos']`).click({ force: true });
  cy.get('.panel-title.collapsed').contains('MÁS OPCIONES DE BÚSQUEDA').click();
});

When("yo busco {string} con el tema {string} ubicado en {string}", (tipo, tema, distrito) => {
  busquedaPage.tipoInstalacion(tipo);
  busquedaPage.distrito(distrito);
  busquedaPage.tema(tema);
});

And("doy click en buscar", () => {
  busquedaPage.botonBuscar();
});

Then("yo obtengo un resultado mayor a {string}", (value) => {
  busquedaPage.validateResult(value);
});
