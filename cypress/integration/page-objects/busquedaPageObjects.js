class Busqueda {
  tipoInstalacion(tipo) {
    cy.get('#tipoInstalacion').type(tipo);
  }

  distrito(distrito) {
    cy.get('#buscaPorDistrito').select(distrito);
  }

  tema(tema) {
    cy.get('#tema').select(tema);
  }

  botonBuscar() {
    cy.get('.button-icon-search').click();
  }

  validateResult(value) {
    cy.get('ul#totalResultsUL li.results-total strong').invoke('text').then(parseInt).should('be.greaterThan', parseInt(value));
  }
}

export default Busqueda;
