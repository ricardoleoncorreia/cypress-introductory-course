# Cypress Introductory Course

This project uses Cypress and Cucumber to implement a basic test case.

To run the project:

* Execute `npm install`.
* Execute `npm run cypress:open`.
